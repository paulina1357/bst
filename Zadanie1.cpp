#include <iostream>

using namespace std;
struct node {
    int x;
    node* rodzic;
    node* lewyPotomek;
    node* prawyPotomek;

    node(int x) {
        this->x = x;
        this->lewyPotomek = nullptr;
        this->prawyPotomek = nullptr;
    }

    ~node() {
        delete lewyPotomek;
        delete prawyPotomek;
    }

};
class BinaryTree {
    node* root;
    int rozmiar;

    void insert(int x, node* aktualny) {
        if(x < aktualny->x) {
            if(aktualny->lewyPotomek != nullptr) {
                insert(x, aktualny->lewyPotomek);
            } else {
                aktualny->lewyPotomek = new node(x);
            }
        } else {
            if(aktualny->prawyPotomek != nullptr) {
                insert(x, aktualny->prawyPotomek);
            } else {
                aktualny->prawyPotomek = new node(x);
            }
        }
    }

    node* searchRecursive(int x, node* aktualny) {
        if(aktualny->x == x) {
            return aktualny;
        } else if(x < aktualny->x && aktualny->lewyPotomek != nullptr) {
            return searchRecursive(x, aktualny->lewyPotomek);
        } else if(x > aktualny->x && aktualny->prawyPotomek != nullptr) {
            return searchRecursive(x, aktualny->prawyPotomek);
        }
        return nullptr;
    }

    int depth(node* aktualny) {
        if(aktualny == nullptr) {
            return 0;
        }
        int lewy = depth(aktualny->lewyPotomek);
        int prawy = depth(aktualny->prawyPotomek);

        if(lewy > prawy) {
            return lewy + 1;
        } else {
            return prawy + 1;
        }
    }

    void printPreorder(node* aktualny) {
        if(aktualny == nullptr) {
            return;
        }
        cout << aktualny->x << endl;
        printPreorder(aktualny->lewyPotomek);
        printPreorder(aktualny->prawyPotomek);
    }

    void printInorder(node* aktualny) {
        if(aktualny == nullptr) {
            return;
        }
        printInorder(aktualny->lewyPotomek);
        cout << aktualny->x << endl;
        printInorder(aktualny->prawyPotomek);
    }

    void printPostorder(node* aktualny) {
        if(aktualny == nullptr) {
            return;
        }
        printPostorder(aktualny->lewyPotomek);
        printPostorder(aktualny->prawyPotomek);
        cout << aktualny->x << endl;
    }

public:
    BinaryTree() {
        rozmiar = 0;
        root = nullptr;
    }

    ~BinaryTree() {
        delete root;
    }

    void insert(int x) {
        if(root == nullptr) {
            root = new node(x);
        } else {
            insert(x, root);
        }
        rozmiar++;
    }



    node* searchRecursive(int x) {
        if(root == nullptr) {
            return nullptr;
        } else {
            return searchRecursive(x, root);
        }
    }

    node* search(int x) {
        node* aktualny = root;

        if(aktualny == nullptr) {
            return nullptr;
        }

        while(true) {
            if(x < aktualny->x) {
                if(aktualny->lewyPotomek == nullptr) {
                    return nullptr;
                }
                aktualny = aktualny->lewyPotomek;
            } else if(x > aktualny->x) {
                if(aktualny->prawyPotomek == nullptr) {
                    return nullptr;
                }
                aktualny = aktualny->prawyPotomek;
            } else {
                return aktualny;
            }
        }
    }

    int size() {
        return rozmiar;
    }

    node* minimum() {
        node* aktualny = root;
        while(aktualny->lewyPotomek != nullptr) {
            aktualny = aktualny->lewyPotomek;
        }
        return aktualny;
    }

    node* maksimum() {
        node* aktualny = root;
        while(aktualny->prawyPotomek != nullptr) {
            aktualny = aktualny->prawyPotomek;
        }
        return aktualny;
    }

    int depth() {
        return depth(root);
    }

    void printInorder() {
        printInorder(root);
    }

    void printPreorder() {
        printPreorder(root);
    }

    void printPostorder() {
        printPostorder(root);
    }
};

int main() {
    BinaryTree binaryTree1;
    int n;
    int x;
    cin >> n;

    while(n--) {
        cin >> x;
        binaryTree1.insert(x);
    }

    binaryTree1.printInorder();
    cout << endl;
    cout << binaryTree1.size() << " " << binaryTree1.depth() << " " << binaryTree1.minimum()->x << " " << binaryTree1.maksimum()->x << endl;

    for (int i = 1; i < 11; ++i) {
        if(binaryTree1.search(i) != nullptr) {
            cout << "Yes" << endl;
        } else {
            cout << "No" << endl;
        }
    }
}