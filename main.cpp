#include "BST.cpp"

#include <iostream>

using namespace std;

int main() {
    BinaryTree<int> binaryTree1;
    int n;
    int x;
//    cin >> n;
//
//    while(n--) {
//        cin >> x;
//        binaryTree1.insert(x);
//    }

    binaryTree1.insert(7);
    binaryTree1.insert(5);
    binaryTree1.insert(9);
    binaryTree1.insert(4);
    binaryTree1.insert(6);
    binaryTree1.insert(8);
    binaryTree1.insert(10);

    binaryTree1.remove(6);
    binaryTree1.remove(6);

    binaryTree1.printPreorder();
    cout << endl;
    binaryTree1.printInorder();
    cout << endl;

    BinaryTree<int> b;
    b = binaryTree1;
    binaryTree1.remove(7);
    binaryTree1.printPreorder();
    cout << endl;
    binaryTree1.printInorder();
    b.printInorder();
    cout << endl;

//    binaryTree1.remove(5);
//    binaryTree1.printInorder();
//
//    binaryTree1.remove(9);
//    binaryTree1.printInorder();
//
//    binaryTree1.remove(7);
//    binaryTree1.printInorder();


    // cout << endl;
   // cout << binaryTree1.size() << " " << binaryTree1.depth() << " " << binaryTree1.minimum()->x << " " << binaryTree1.maksimum()->x << endl;

//    for (int i = 1; i < 11; ++i) {
//        if(binaryTree1.search(i) != nullptr) {
//            cout << "Yes" << endl;
//        } else {
//            cout << "No" << endl;
//        }
//    }
}
