#include "BST.h"

#include <iostream>
#include <cstdlib>

using namespace std;

template<typename T>
node<T>::node(T x) {
    this->x = x;
    this->lewyPotomek = nullptr;
    this->prawyPotomek = nullptr;
}

template<typename T>
node<T>::~node() {
    delete lewyPotomek;
    delete prawyPotomek;
}

template<typename T>
BinaryTree<T>::BinaryTree(const BinaryTree<T> &tree) {
//    cout << "Konstruktor kopiujacy" << endl;

    if (tree.root != nullptr) {
        root = new node<T>(tree.root->x);
        copy(root, tree.root);
    }
}

template<typename T>
BinaryTree<T>::BinaryTree(BinaryTree<T> &&tree) {
    root = tree.root;
    rozmiar = tree.rozmiar;
    tree.root = nullptr;
    tree.rozmiar = nullptr;
    cout << "Konstruktor przenoszacy" << endl;
}

template<typename T>
void BinaryTree<T>::copy(node<T> *inThis, node<T> *inOther) {
    if (inOther->lewyPotomek != nullptr) {
        inThis->lewyPotomek = new node<T>(inOther->lewyPotomek->x);
        copy(inThis->lewyPotomek, inOther->lewyPotomek);
    }

    if (inOther->prawyPotomek != nullptr) {
        inThis->prawyPotomek = new node<T>(inOther->prawyPotomek->x);
        copy(inThis->prawyPotomek, inOther->prawyPotomek);
    }
}

template<typename T>
void BinaryTree<T>::insert(T x, node<T> *aktualny) {
    if (x < aktualny->x) {
        if (aktualny->lewyPotomek != nullptr) {
            insert(x, aktualny->lewyPotomek);
        } else {
            aktualny->lewyPotomek = new node<T>(x);
        }
    } else {
        if (aktualny->prawyPotomek != nullptr) {
            insert(x, aktualny->prawyPotomek);
        } else {
            aktualny->prawyPotomek = new node<T>(x);
        }
    }
}

//    template <typename T>
//    node<T>* BinaryTree<T>::najmniejszy(node<T>* aktualny) {
//        if(aktualny->lewyPotomek != nullptr) {
//            return najmniejszy(aktualny->lewyPotomek);
//        }
//    }


template<typename T>
node<T> *BinaryTree<T>::searchRecursive(T x, node<T> *aktualny) {
    if (aktualny->x == x) {
        return aktualny;
    } else if (x < aktualny->x && aktualny->lewyPotomek != nullptr) {
        return searchRecursive(x, aktualny->lewyPotomek);
    } else if (x > aktualny->x && aktualny->prawyPotomek != nullptr) {
        return searchRecursive(x, aktualny->prawyPotomek);
    }
    return nullptr;
}

template<typename T>
T BinaryTree<T>::depth(node<T> *aktualny) {
    if (aktualny == nullptr) {
        return 0;
    }
    T lewy = depth(aktualny->lewyPotomek);
    T prawy = depth(aktualny->prawyPotomek);

    if (lewy > prawy) {
        return lewy + 1;
    } else {
        return prawy + 1;
    }
}

template<typename T>
void BinaryTree<T>::printPreorder(node<T> *aktualny) {
    if (aktualny == nullptr) {
        return;
    }
    cout << aktualny->x << " ";
    printPreorder(aktualny->lewyPotomek);
    printPreorder(aktualny->prawyPotomek);
}

template<typename T>
void BinaryTree<T>::printInorder(node<T> *aktualny) {
    if (aktualny == nullptr) {
        return;
    }
    printInorder(aktualny->lewyPotomek);
    cout << aktualny->x << " ";
    printInorder(aktualny->prawyPotomek);
}

template<typename T>
void BinaryTree<T>::printPostorder(node<T> *aktualny) {
    if (aktualny == nullptr) {
        return;
    }
    printPostorder(aktualny->lewyPotomek);
    printPostorder(aktualny->prawyPotomek);
    cout << aktualny->x << endl;
}

template<typename T>
BinaryTree<T>::BinaryTree() {
    rozmiar = 0;
    root = nullptr;
}

template<typename T>
BinaryTree<T>::~BinaryTree() {
    delete root;
}

template<typename T>
void BinaryTree<T>::insert(T x) {
    if (root == nullptr) {
        root = new node<T>(x);
    } else {
        insert(x, root);
    }
    rozmiar++;
}

template<typename T>
void BinaryTree<T>::remove(T x) {
    if (root == nullptr) {
        cout << "Drzewo jest puste" << endl;
        return;
    }

    node<T>* parent = nullptr;
    node<T>* toRemove = root;

    while (toRemove->x != x) {
        parent = toRemove;
        if (x < toRemove->x) {
            toRemove = toRemove->lewyPotomek;
        } else {
            toRemove = toRemove->prawyPotomek;
        }

        if (toRemove == nullptr) {
            return;
        }
    }

    if (toRemove->lewyPotomek == nullptr) {
        if (parent == nullptr) {
            root = toRemove->prawyPotomek;
        } else if (parent->lewyPotomek == toRemove) {
            parent->lewyPotomek = toRemove->prawyPotomek;
        } else {
            parent->prawyPotomek = toRemove->prawyPotomek;
        }

        toRemove->prawyPotomek = nullptr;
        delete toRemove;

    } else {
        node<T>* newNode = toRemove->lewyPotomek;
        node<T>* newNodeParent = nullptr;

        while (newNode->prawyPotomek != nullptr) {
            newNodeParent = newNode;
            newNode = newNode->prawyPotomek;
        }

        if (newNodeParent != nullptr) {
            newNodeParent->prawyPotomek = newNode->lewyPotomek;
            newNode->lewyPotomek = toRemove->lewyPotomek;
        }
        newNode->prawyPotomek = toRemove->prawyPotomek;

        if (parent == nullptr) {
            root = newNode;
        } else if (parent->lewyPotomek == toRemove) {
            parent->lewyPotomek = newNode;
        } else {
            parent->prawyPotomek = newNode;
        }

        toRemove->lewyPotomek = nullptr;
        toRemove->prawyPotomek = nullptr;
        delete toRemove;
    }

    rozmiar--;
}


template<typename T>
node<T> *BinaryTree<T>::searchRecursive(T x) {
    if (root == nullptr) {
        return nullptr;
    } else {
        return searchRecursive(x, root);
    }
}

template<typename T>
node<T> *BinaryTree<T>::search(T x) {
    node<T> *aktualny = root;

    if (aktualny == nullptr) {
        return nullptr;
    }

    while (true) {
        if (x < aktualny->x) {
            if (aktualny->lewyPotomek == nullptr) {
                return nullptr;
            }
            aktualny = aktualny->lewyPotomek;
        } else if (x > aktualny->x) {
            if (aktualny->prawyPotomek == nullptr) {
                return nullptr;
            }
            aktualny = aktualny->prawyPotomek;
        } else {
            return aktualny;
        }
    }
}

template<typename T>
T BinaryTree<T>::size() {
    return rozmiar;
}

template<typename T>
node<T> *BinaryTree<T>::minimum() {
    node<T> *aktualny = root;
    while (aktualny->lewyPotomek != nullptr) {
        aktualny = aktualny->lewyPotomek;
    }
    return aktualny;
}

template<typename T>
void BinaryTree<T>::minimum(node<T> *aktualny) {
    node<T> *minimum = aktualny;

    while (minimum->lewyPotomek != nullptr) {
        minimum = minimum->lewyPotomek;
    }

    //return minimum;
}

template<typename T>
node<T> *BinaryTree<T>::maksimum() {
    node<T> *aktualny = root;
    while (aktualny->prawyPotomek != nullptr) {
        aktualny = aktualny->prawyPotomek;
    }
    return aktualny;
}

template<typename T>
T BinaryTree<T>::depth() {
    return depth(root);
}

template<typename T>
void BinaryTree<T>::printInorder() {
    printInorder(root);
}

template<typename T>
void BinaryTree<T>::printPreorder() {
    printPreorder(root);
}

template<typename T>
void BinaryTree<T>::printPostorder() {
    printPostorder(root);
}

