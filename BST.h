#ifndef BST_BST_H
#define BST_BST_H
#include <iostream>
#include <windows.h>
#include <string>

using namespace std;

template <typename T>
struct node {
    T x;
    node* lewyPotomek;
    node* prawyPotomek;

    node(T x);
    ~node();
};

template <typename T>
class BinaryTree {
    node<T>* root;
    T rozmiar;

    void insert(T x, node<T>* aktualny);
    node<T>* searchRecursive(T x, node<T>* aktualny);
    T depth(node<T>* aktualny);
    void printPreorder(node<T>* aktualny);
    void printInorder(node<T>* aktualny);
    void printPostorder(node<T>* aktualny);
    void print(node<T>* aktualny, T h, T m);
    void copy(node<T>* inThis, node<T>* inOther);

public:
    BinaryTree();
    BinaryTree(const BinaryTree<T> &tree); //konstruktor kopiujacy
    BinaryTree(BinaryTree<T> &&tree); //konstruktor prznoszacy
    ~BinaryTree();

    void insert(T x);

    void remove(T x);
    node<T>* searchRecursive(T x);
    node<T>* search(T x);
    T size();
    node<T>* minimum();
    void minimum(node<T>* aktualny);
    node<T>* maksimum();
    T depth();
    void najmniejszy();
    void printInorder();
    void printPreorder();
    void printPostorder();
};
#endif //BST_BST_H




